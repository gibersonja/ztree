package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"
	"runtime"
)

func main() {
	if isPipe() {
		var stdin []byte
		stdin = append(stdin, 32)
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			stdin = append(stdin, scanner.Bytes()...)
		}
		err := scanner.Err()
		er(err)

		var items []string

		for i := 0; i < len(stdin); i++ {
			if i+1 < len(stdin) && stdin[i] == 45 && stdin[i+1] == 32 {
				items = append(items, string(stdin[i]))
				continue
			}

			if stdin[i] != 32 && stdin[i-1] == 32 {
				for j := i; j < len(stdin); j++ {
					if stdin[j] != 32 && stdin[j] != 45 && j+1 < len(stdin) && (stdin[j+1] == 32 || stdin[j+1] == 45) {
						items = append(items, string(stdin[i:j+1]))
						break
					}
				}
			}
		}

		var zfs map[int][]string
		zfs = make(map[int][]string)
		ctr := 0

		for k := 0; k < len(items); k++ {
			item := items[k]

			if item == "-" {
				if k+1 < len(items) {
					zfs[ctr] = append(zfs[ctr], items[k+1])
					ctr = ctr + 1
				} else {
					er(fmt.Errorf("orign with no child"))
				}
			}
		}

		fmt.Println(zfs)

	} else {
		help()
	}
}

func help() {
	exe, err := os.Executable()
	er(err)
	fmt.Print("Parse output of zorigin (alias) to a tree hierarchy via STDIN named pipe\n")
	fmt.Printf("Usage: zfs list -r -t filesystem -o origin,name | %s\n", filepath.Base(exe))
	return
}

func isPipe() bool {
	fileInfo, _ := os.Stdin.Stat()
	return fileInfo.Mode()&os.ModeCharDevice == 0
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("Error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(),
			path.Base(file), line, err)
	}
}
